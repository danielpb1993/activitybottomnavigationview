package com.example.activitatbottomnavigationview;

public class Picture {
    private String urlImatge;
    private String namePic;
    private String description;

    public Picture(String urlImatge, String namePic, String description) {
        this.urlImatge = urlImatge;
        this.namePic = namePic;
        this.description = description;
    }

    public String getUrlImatge() {
        return urlImatge;
    }

    public void setUrlImatge(String urlImatge) {
        this.urlImatge = urlImatge;
    }

    public String getNamePic() {
        return namePic;
    }

    public void setNamePic(String namePic) {
        this.namePic = namePic;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

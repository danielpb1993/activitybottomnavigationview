package com.example.activitatbottomnavigationview;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {
    ArrayList<Picture> pictures = new ArrayList<>();

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);

        initData();
        MyAdapter myAdapter = new MyAdapter(pictures, getContext());
        recyclerView.setAdapter(myAdapter);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3, GridLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(gridLayoutManager);

        return view;
    }

    private void initData() {
        Picture picture1 = new Picture("https://joanseculi.com/images/img01.jpg", "Pic01", "Description" );
        Picture picture2 = new Picture("https://joanseculi.com/images/img02.jpg", "Pic02", "Description" );
        Picture picture3 = new Picture("https://joanseculi.com/images/img03.jpg", "Pic03", "Description" );
        Picture picture4 = new Picture("https://joanseculi.com/images/img04.jpg", "Pic04", "Description" );
        Picture picture5 = new Picture("https://joanseculi.com/images/img05.jpg", "Pic05", "Description" );
        Picture picture6 = new Picture("https://joanseculi.com/images/img06.jpg", "Pic06", "Description" );
        Picture picture7 = new Picture("https://joanseculi.com/images/img07.jpg", "Pic07", "Description" );
        Picture picture8 = new Picture("https://joanseculi.com/images/img08.jpg", "Pic08", "Description" );
        Picture picture9 = new Picture("https://joanseculi.com/images/img09.jpg", "Pic09", "Description" );
        Picture picture10 = new Picture("https://joanseculi.com/images/img10.jpg", "Pic10", "Description" );
        Picture picture11 = new Picture("https://joanseculi.com/images/img11.jpg", "Pic11", "Description" );
        Picture picture12 = new Picture("https://joanseculi.com/images/img12.jpg", "Pic12", "Description" );
        Picture picture13 = new Picture("https://joanseculi.com/images/img13.jpg", "Pic13", "Description" );
        Picture picture14 = new Picture("https://joanseculi.com/images/img14.jpg", "Pic14", "Description" );
        Picture picture15 = new Picture("https://joanseculi.com/images/img15.jpg", "Pic15", "Description" );
        Picture picture16 = new Picture("https://joanseculi.com/images/img16.jpg", "Pic16", "Description" );
        Picture picture17 = new Picture("https://joanseculi.com/images/img17.jpg", "Pic17", "Description" );
        Picture picture18 = new Picture("https://joanseculi.com/images/img18.jpg", "Pic18", "Description" );

        pictures.add(picture1);
        pictures.add(picture2);
        pictures.add(picture3);
        pictures.add(picture4);
        pictures.add(picture5);
        pictures.add(picture6);
        pictures.add(picture7);
        pictures.add(picture8);
        pictures.add(picture9);
        pictures.add(picture10);
        pictures.add(picture11);
        pictures.add(picture12);
        pictures.add(picture13);
        pictures.add(picture14);
        pictures.add(picture15);
        pictures.add(picture16);
        pictures.add(picture17);
        pictures.add(picture18);
    }
}
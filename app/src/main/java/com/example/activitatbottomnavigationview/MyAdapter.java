package com.example.activitatbottomnavigationview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {
    private ArrayList<Picture> mPictures;
    private Context mContext;

    public MyAdapter(ArrayList<Picture> mPictures, Context mContext) {
        this.mPictures = mPictures;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(mContext);
        View view = layoutInflater.inflate(R.layout.my_row, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Picasso.get().load(mPictures.get(position).getUrlImatge())
                .fit()
                .centerCrop()
                .into(holder.imageView);
        holder.namePic.setText(mPictures.get(position).getNamePic());
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(v.getContext(), mPictures.get(holder.getAdapterPosition()).getNamePic(), Toast.LENGTH_SHORT).show();

                Fragment pictureDetailFragment = PictureDetailFragment.newInstance(
                        mPictures.get(holder.getAdapterPosition()).getNamePic(),
                        mPictures.get(holder.getAdapterPosition()).getDescription(),
                        mPictures.get(holder.getAdapterPosition()).getUrlImatge()
                );
                FragmentManager fragmentManager = ((AppCompatActivity)mContext).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.main_frame, pictureDetailFragment).addToBackStack("main_frame");
                fragmentTransaction.commit();

//                fragmentTransaction.hide(((AppCompatActivity)mContext).getSupportFragmentManager().findFragmentByTag("main_frame"));
//                fragmentTransaction.add(R.id.main_frame, pictureDetailFragment);
//                fragmentTransaction.addToBackStack(null);
//                fragmentTransaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPictures.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageView;
        private TextView namePic;
        ConstraintLayout rowLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            namePic = itemView.findViewById(R.id.namePic);
            rowLayout = itemView.findViewById(R.id.rowLayout);
        }
    }
}
